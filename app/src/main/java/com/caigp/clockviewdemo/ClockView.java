package com.caigp.clockviewdemo;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import androidx.annotation.Nullable;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class ClockView extends View {

    private Paint mPaint;

    private SimpleDateFormat sdf;

    //表盘半径
    private float radius;

    private int w;

    private int h;

    private int timeColor;

    private float textSize;

    public ClockView(Context context) {
        this(context, null);
    }

    public ClockView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ClockView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        sdf = new SimpleDateFormat("HH:mm:ss", Locale.CHINESE);

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.ClockView);
        timeColor = ta.getColor(R.styleable.ClockView_timeColor, 0xFF000000);
        textSize = ta.getFloat(R.styleable.ClockView_textSize, 55);
        float clockStrokeWidth = ta.getFloat(R.styleable.ClockView_clockStrokeWidth, 5);
        ta.recycle();

        mPaint = new Paint();
        mPaint.setStrokeWidth(clockStrokeWidth);
        mPaint.setAntiAlias(true);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);

        w = width;
        h = height;

        radius = Math.min(width, height) / 2.0f - mPaint.getStrokeWidth() / 2.0f;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        this.w = w;
        this.h = h;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mPaint.setColor(0xFF000000);

        //画圆
        mPaint.setStyle(Paint.Style.STROKE);
        canvas.drawCircle(getWidth() / 2, getHeight() / 2, radius, mPaint);

        //画刻度
        if (w < h) {
            for (int i = 0; i < 60; i++) {
                if (i % 5 == 0) {
                    //canvas.drawLine(getWidth()/2-2, getHeight()/2- getWidth()/2, getWidth()/2+2, getHeight()/2- getWidth()/2 + 50, mPaint);
                    canvas.drawLine(getWidth() / 2, getHeight() / 2 - getWidth() / 2, getWidth() / 2, getHeight() / 2.0f - radius / 10 * 8, mPaint);
                    canvas.rotate(6.0f, getWidth() / 2, getHeight() / 2);
                    continue;
                }
                canvas.drawLine(getWidth() / 2, getHeight() / 2- getWidth() / 2, getWidth() / 2, getHeight() / 2.0f - radius / 10 * 9, mPaint);

                canvas.rotate(6.0f, getWidth() / 2, getHeight() / 2);
            }
        } else {
            for (int i = 0; i < 60; i++) {
                if (i % 5 == 0) {
                    //canvas.drawLine(getWidth()/2-2, getHeight()/2- getWidth()/2, getWidth()/2+2, getHeight()/2- getWidth()/2 + 50, mPaint);
                    canvas.drawLine(getWidth() / 2, 0, getWidth() / 2, getHeight() / 2.0f - radius / 10 * 8, mPaint);
                    canvas.rotate(6.0f, getWidth() / 2, getHeight() / 2);
                    continue;
                }
                canvas.drawLine(getWidth() / 2, 0, getWidth() / 2, getHeight() / 2.0f - radius / 10 * 9, mPaint);

                canvas.rotate(6.0f, getWidth() / 2, getHeight() / 2);
            }
        }

        //画数字
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setTextSize(textSize);
        for (int i = 0; i < 12; i++) {
            canvas.save();
            String num = i + 1 + "";
            canvas.rotate(30.0f*(i+1), getWidth()/2, getHeight()/2);
            canvas.rotate(360 - 30*(i+1), getWidth() / 2.0f, getHeight() / 2.0f - radius / 10 * 7 - (-(mPaint.ascent() + mPaint.descent())/2));

            canvas.drawText(num, getWidth() / 2.0f - mPaint.measureText(num) / 2.0f, getHeight() / 2.0f - radius / 10 * 7, mPaint);
            canvas.restore();
        }

        //画中心点
        canvas.drawCircle(getWidth()/2, getHeight()/2, 20, mPaint);

        long currentTimeMillis = System.currentTimeMillis();

        String format = sdf.format(currentTimeMillis);
        String[] split = format.split(":");

        int hour = Integer.parseInt(split[0]);
        int minute = Integer.parseInt(split[1]);
        int second = Integer.parseInt(split[2]);

        mPaint.setColor(timeColor);
        canvas.drawText(format, getWidth() / 2.0f - mPaint.measureText(format) / 2, getHeight() / 2.0f + radius / 3, mPaint);

        mPaint.setColor(0xFF000000);
        //时针
        canvas.save();
        canvas.rotate(hour * 30 + minute/2, getWidth()/2, getHeight()/2);
        canvas.drawLine(getWidth() / 2, getHeight() / 2.0f - radius / 10 * 4, getWidth()/2, getHeight()/2.0f + radius / 10, mPaint);

        canvas.restore();
        canvas.save();
        //分针
        canvas.rotate(6.0f * minute, getWidth()/2, getHeight()/2);
        canvas.drawLine(getWidth()/2, getHeight() / 2.0f - radius / 10 * 7, getWidth()/2, getHeight()/2.0f + radius / 10, mPaint);

        canvas.restore();
        //秒针
        canvas.rotate(6.0f * second, getWidth()/2, getHeight()/2);
        canvas.drawLine(getWidth()/2, getHeight() / 2.0f - radius / 10 * 8, getWidth()/2, getHeight()/2.0f + radius / 10 * 2, mPaint);

        postInvalidateDelayed(1000);
    }
}
